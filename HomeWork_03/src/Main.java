import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter array length: ");
        int length = scanner.nextInt();

        int[] array = new int[length];
        int localMinCounter = 0;

        System.out.print("Enter elements of array: ");

        for (int i = 0; i < array.length; i++) {
            array[i] = scanner.nextInt();
        }

        if (array[0] < array[1]) {
            localMinCounter++;
        }

        for (int i = 1; i < array.length - 1; i++) {
            if (array[i] < array[i-1] && array[i] < array[i+1]) {
                localMinCounter++;
            }
        }

        if (array[array.length - 1] < array[array.length - 2]) {
            localMinCounter++;
        }

        System.out.println("Number of local minimum: " + localMinCounter);
    }

}