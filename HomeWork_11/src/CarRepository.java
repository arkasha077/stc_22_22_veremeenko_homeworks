import java.util.List;

public interface CarRepository {
    List<Car> findByColor(String color);
    List<Car> findByMileage(Integer mileage);
    List<String> findUniqueModelsByPriceRange(Integer priceFrom, Integer priceTo);
    String findColorWithMinPrice();
    Double findAveragePriceByModel(String model);
}
