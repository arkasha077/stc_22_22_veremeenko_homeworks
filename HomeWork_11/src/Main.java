public class Main {
    public static void main(String[] args) {
        CarRepository cars = new CarRepositoryFileBasedImpl("src/input.txt");
        System.out.println(cars.findByColor("Green"));
        System.out.println(cars.findByMileage(133));
        System.out.println(cars.findUniqueModelsByPriceRange(80000, 170000));
        System.out.println(cars.findColorWithMinPrice());
        System.out.println(cars.findAveragePriceByModel("Camry"));
    }
}