import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;

public class CarRepositoryFileBasedImpl implements CarRepository {
    private final String fileName;

    public CarRepositoryFileBasedImpl(String fileName) {
        this.fileName = fileName;
    }

    public static final Function<String, Car> stringToCarMapper = car -> {
        String[] parts = car.split("\\|");
        String number = parts[0];
        String model = parts[1];
        String color = parts[2];
        Integer mileage = Integer.parseInt(parts[3]);
        Integer price = Integer.parseInt(parts[4]);

        return new Car(number, model, color, mileage, price);
    };

    @Override
    public List<Car> findByColor(String color) {
        try {
            return new BufferedReader(new FileReader(fileName))
                    .lines()
                    .map(stringToCarMapper)
                    .filter(car -> car.getColor().equals(color))
                    .toList();
        }
        catch (IOException e) {
            throw new UnsuccessfulWorkWithFileException(e);
        }
    }

    @Override
    public List<Car> findByMileage(Integer mileage) {
        try {
            return new BufferedReader(new FileReader(fileName))
                    .lines()
                    .map(stringToCarMapper)
                    .filter(car -> car.getMileage().equals(mileage))
                    .toList();
        }
        catch (IOException e) {
            throw new UnsuccessfulWorkWithFileException(e);
        }
    }

    @Override
    public List<String> findUniqueModelsByPriceRange(Integer priceFrom, Integer priceTo) {
        try {
            return new BufferedReader(new FileReader(fileName))
                    .lines()
                    .map(stringToCarMapper)
                    .filter(car -> car.getPrice() >= priceFrom && car.getPrice() <= priceTo)
                    .map(Car::getModel)
                    .distinct()
                    .toList();
        }
        catch (IOException e) {
            throw new UnsuccessfulWorkWithFileException(e);
        }
    }

    @Override
    public String findColorWithMinPrice() {
        try {
            return new BufferedReader(new FileReader(fileName))
                    .lines()
                    .map(stringToCarMapper)
                    .min(Comparator.comparingInt(Car::getPrice))
                    .map(Car::getColor)
                    .get();
        }
        catch (IOException e) {
            throw new UnsuccessfulWorkWithFileException(e);
        }
    }

    @Override
    public Double findAveragePriceByModel(String model) {
        try {
            return new BufferedReader(new FileReader(fileName))
                    .lines()
                    .map(stringToCarMapper)
                    .filter(car -> car.getModel().equals(model))
                    .mapToInt(Car::getPrice)
                    .average()
                    .getAsDouble();
        }
        catch (IOException e) {
            throw new UnsuccessfulWorkWithFileException(e);
        }
    }
}
