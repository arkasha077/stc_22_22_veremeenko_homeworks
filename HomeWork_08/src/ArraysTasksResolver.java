public class ArraysTasksResolver {

    public static void resolveTask(int[] array, ArrayTask task, int from, int to) {

        System.out.print("\nИсходный массив: ");

        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }

        System.out.printf("\nfrom = %d, to = %d", from, to);

        System.out.println("\nРезультат: " + task.resolve(array, from, to));
    }
}
