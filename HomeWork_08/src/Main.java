public class Main {

    public static void main(String[] args) {

        int[] numbers = {1, 2, 3, 4, 5, 6, 7, 8};
        int[] numbers2 = {12, 62, 4, 2, 100, 40, 56};

        ArrayTask sumOfNumbers = (array, from, to) -> {

            int sum = 0;

            for (int i = from; i <= to; i++) {
                sum += array[i];
            }

            return sum;
        };

        ArrayTask sumOfDigits = (array, from, to) -> {

            int max = array[from];

            for (int i = from + 1; i <= to; i++) {
                if (array[i] > max) {
                    max = array[i];
                }
            }

            int sum = 0;

            while (max != 0) {
                sum += max % 10;
                max /= 10;
            }

            return sum;
        };

        ArraysTasksResolver.resolveTask(numbers, sumOfNumbers, 0, 2);
        ArraysTasksResolver.resolveTask(numbers2, sumOfDigits, 1, 3);
    }
}