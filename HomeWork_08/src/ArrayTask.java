public interface ArrayTask {
    public int resolve(int[] array, int from, int to);
}
