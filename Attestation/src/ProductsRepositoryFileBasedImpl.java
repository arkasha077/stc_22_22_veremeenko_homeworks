import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.function.Function;

public class ProductsRepositoryFileBasedImpl implements ProductsRepository {
    private final String fileName;

    public ProductsRepositoryFileBasedImpl(String fileName) {
        this.fileName = fileName;
    }

    public static final Function<String, Product> stringToProductMapper = product -> {
        String[] parts = product.split("\\|");
        Integer id = Integer.parseInt(parts[0]);
        String name = parts[1];
        Double price = Double.parseDouble(parts[2]);
        Integer amount = Integer.parseInt(parts[3]);
        return new Product(id, name, price, amount);
    };

    @Override
    public Product findById(Integer id) {
        try {
            return new BufferedReader(new FileReader(fileName))
                    .lines()
                    .map(stringToProductMapper)
                    .filter(product -> product.getId().equals(id))
                    .findFirst()
                    .get();
        }
        catch (IOException e) {
            throw new UnsuccessfulWorkWithFileException(e);
        }
    }

    @Override
    public List<Product> findAllByTitleLike(String title) {
        try {
            return new BufferedReader(new FileReader(fileName))
                    .lines()
                    .map(stringToProductMapper)
                    .filter(product -> product.getName().contains(title))
                    .toList();
        }
        catch (IOException e) {
            throw new UnsuccessfulWorkWithFileException(e);
        }
    }
}
