public class Main {
    public static void main(String[] args) {
        ProductsRepository productsRepository = new ProductsRepositoryFileBasedImpl("src/input.txt");
        System.out.println(productsRepository.findById(3));
        System.out.println(productsRepository.findAllByTitleLike("Сыр"));
    }
}