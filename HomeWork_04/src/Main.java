import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Main {

    public static int[] generateRandomArray(int bound, int length) {

        Random random = new Random();
        int[] array = new int[length];

        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(bound);
        }

        return array;
    }

    public static int calcSumOfArrayRange(int[] array, int from, int to) {

        if (from > to) {
            return -1;
        }

        int sum = 0;

        for (int i = from; i <= to; i++) {
            sum += array[i];
        }

        return sum;
    }

    public static void printEvenNumbersOfArray(int[] array) {

        System.out.print("Even numbers: ");

        for (int i = 0; i < array.length; i++) {
            if (array[i] % 2 == 0) {
                System.out.print(array[i] + " ");
            }
        }

        System.out.println();
    }

    public static int convertToInt(int[] digits) {

        int result = 0;
        int exp;

        for (int i = 0; i < digits.length; i++) {
            exp = (digits.length - 1) - i;
            result += digits[i] * Math.pow(10, exp);
        }

        return result;
    }

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter the length of array: ");
        int arrayLength = scanner.nextInt();

        System.out.print("Enter the bound of array element: ");
        int bound = scanner.nextInt();

        int[] array = generateRandomArray(bound, arrayLength);

        System.out.println("Array: " + Arrays.toString(array));

        printEvenNumbersOfArray(array);

        System.out.print("Enter index 'from': ");
        int from = scanner.nextInt();

        System.out.print("Enter index 'to': ");
        int to = scanner.nextInt();

        int sum = calcSumOfArrayRange(array, from, to);
        System.out.println(String.format("Sum of element from %s to %s: %s", from, to, sum));

        int[] digits = generateRandomArray(10, 5);

        System.out.println("Digits: " + Arrays.toString(digits));
        System.out.println("Number: " + convertToInt(digits));
    }
}