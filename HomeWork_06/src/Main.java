public class Main {
    public static void main(String[] args) {
        Rectangle rectangle = new Rectangle(10, 15, 35, 50);
        Square square = new Square(1, 5, 13);
        Ellipse ellipse = new Ellipse(2, 4, 8, 16);
        Circle circle = new Circle(7, 7, 14);

        System.out.printf("Rectangle: perimeter = %.2f; area = %.2f \n", rectangle.getPerimeter(), rectangle.getArea());
        System.out.printf("Square: perimeter = %.2f; area = %.2f \n", square.getPerimeter(), square.getArea());
        System.out.printf("Ellipse: perimeter = %.2f; area = %.2f \n", ellipse.getPerimeter(), ellipse.getArea());
        System.out.printf("Circle: perimeter = %.2f; area = %.2f \n", circle.getPerimeter(), circle.getArea());
    }
}