public class Ellipse extends Shape {
    private double smallRadius;
    private double largeRadius;

    public Ellipse(double centerX, double centerY, double smallRadius, double largeRadius) {
        super(centerX, centerY);
        this.smallRadius = smallRadius;
        this.largeRadius = largeRadius;
    }

    @Override
    public double getPerimeter() {
        return Math.PI * (smallRadius + largeRadius) ;
    }

    @Override
    public double getArea() {
        return Math.PI * smallRadius * largeRadius;
    }
}
