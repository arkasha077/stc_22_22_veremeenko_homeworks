public abstract class Shape {
    private double centerX;
    private double centerY;

    public Shape(double centerX, double centerY) {
        this.centerX = centerX;
        this.centerY = centerY;
    }

    public abstract double getPerimeter();
    public abstract double getArea();

    public void move(double newX, double newY) {
        centerX = newX;
        centerY = newY;
    }

    public double getCenterX() {
        return centerX;
    }

    public double getCenterY() {
        return centerY;
    }
}
