public class Rectangle extends Shape {
    private double height;
    private double length;

    public Rectangle(double centerX, double centerY, double height, double length) {
        super(centerX, centerY);
        this.height = height;
        this.length = length;
    }

    @Override
    public double getPerimeter() {
        return 2 * (height + length);
    }

    @Override
    public double getArea() {
        return height * length;
    }

}
