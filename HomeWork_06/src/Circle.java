public class Circle extends Ellipse {
    public Circle(double centerX, double centerY, double radius) {
        super(centerX, centerY, radius, radius);
    }
}
