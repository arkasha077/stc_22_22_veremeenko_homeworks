public class Main {

    public static void main(String[] args) {
        EvenNumbersPrintTask evenNumbers = new EvenNumbersPrintTask(1, 10);
        OddNumbersPrintTask oddNumbers = new OddNumbersPrintTask(11, 20);

        Task[] tasks = {evenNumbers, oddNumbers};

        completeAllTasks(tasks);
    }

    public static void completeAllTasks(Task[] tasks) {
        for (int i = 0; i < tasks.length; i++) {
            tasks[i].complete();
        }
    }
}