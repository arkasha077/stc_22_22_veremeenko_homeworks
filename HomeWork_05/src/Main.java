import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Atm atm = new Atm();
        Scanner scanner = new Scanner(System.in);

        boolean isWork = true;

        String balance;
        String transactions;
        String menu = "\nМеню операций: 1-снятие наличных, 2-пополнение, 3-выйти. Лимит на снятие: " + atm.MAX_MONEY_OUT;

        while (isWork) {
            balance = "\nДенег в банкомате: " + atm.getMoney();
            transactions = "\nПроведено операций: " + atm.getTransactionCounter();

            System.out.println(balance + transactions + menu);
            System.out.print("\nВыберите операцию: ");

            switch (scanner.nextInt()) {
                case (1):
                    getCash(atm, scanner);
                    break;
                case (2):
                    putCash(atm, scanner);
                    break;
                case (3):
                    isWork = false;
                    break;
            }
        }

    }

    public static void getCash(Atm atm, Scanner scanner) {
        System.out.print("Введите сумму для снятия: ");
        int cash = atm.giveCash(scanner.nextInt());
        System.out.println("Снято наличных: " + cash);

        if (cash == 0) {
            System.out.println("Превышен допустимый лимит или закончились средства в банкомате!");
        }
    }

    public static void putCash(Atm atm, Scanner scanner) {
        System.out.print("Внесите сумму для пополнения: ");
        int change = atm.putCash(scanner.nextInt());
        System.out.println("Счет пополнен.");

        if (change > 0) {
            System.out.println("Достигнута максимальная сумма на балансе. Часть средств возвращена вам: " + change);
        }
    }
}