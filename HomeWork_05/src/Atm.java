public class Atm {

    public final static int MAX_MONEY_VALUE = 1000000;
    public final static int MAX_MONEY_OUT = 200000;

    private int transactionCounter;
    private int money;

    public Atm() {
        transactionCounter = 0;
        money = MAX_MONEY_VALUE;
    }

    public int giveCash(int amount) {
        if (amount <= MAX_MONEY_OUT) {
            if (amount <= money) {
                money -= amount;
                transactionCounter++;
                return amount;
            } else if (amount > money) {
                int availableMoney = money;
                money = 0;
                transactionCounter++;
                return availableMoney;
            }
        }

        return 0;
    }

    public int putCash(int amount) {
        int resultMoney = money + amount;

        if (resultMoney <= MAX_MONEY_VALUE) {
            money = resultMoney;
            transactionCounter++;
            return 0;
        } else {
            // change - сдача
            int change = amount - (MAX_MONEY_VALUE - money);
            money = MAX_MONEY_VALUE;
            transactionCounter++;
            return change;
        }
    }

    public int getMoney() {
        return money;
    }

    public int getTransactionCounter() {
        return transactionCounter;
    }
}
