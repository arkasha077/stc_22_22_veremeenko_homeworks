package ru.inno.cinema.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.inno.cinema.models.Playlist;
import ru.inno.cinema.models.User;
import ru.inno.cinema.repositories.PlaylistsRepository;
import ru.inno.cinema.repositories.UsersRepository;
import ru.inno.cinema.security.details.CustomUserDetails;
import ru.inno.cinema.services.ProfileService;

import java.util.Set;

@RequiredArgsConstructor
@Service
public class ProfileServiceImpl implements ProfileService {

    private final UsersRepository usersRepository;
    private final PlaylistsRepository playlistsRepository;

    @Override
    public User getCurrent(CustomUserDetails userDetails) {
        return usersRepository.findById(userDetails.getUserId()).orElseThrow();
    }

    @Override
    public void deletePlaylist(CustomUserDetails userDetails, Long playlistId) {
        Playlist playlist = playlistsRepository.findById(playlistId).orElseThrow();
        User user = getCurrent(userDetails);

        user.getPlaylists().remove(playlist);

        usersRepository.save(user);
    }
}
