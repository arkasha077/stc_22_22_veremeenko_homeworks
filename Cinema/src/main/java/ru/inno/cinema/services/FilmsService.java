package ru.inno.cinema.services;

import ru.inno.cinema.dto.FilmForm;
import ru.inno.cinema.models.Film;

import java.util.List;

public interface FilmsService {
    List<Film> getAllFilms();

    void addFilm(FilmForm film);

    Film getFilm(Long id);

    void updateFilm(Long id, FilmForm film);

    void deleteFilm(Long id);
}
