package ru.inno.cinema.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.inno.cinema.dto.FilmForm;
import ru.inno.cinema.models.Film;
import ru.inno.cinema.repositories.FilmsRepository;
import ru.inno.cinema.services.FilmsService;

import java.util.List;

@Service
@RequiredArgsConstructor
public class FilmsServiceImpl implements FilmsService {

    private final FilmsRepository filmsRepository;

    @Override
    public List<Film> getAllFilms() {
        return filmsRepository.findAllByStateNot(Film.State.DELETED);
    }

    @Override
    public void addFilm(FilmForm film) {
        Film newFilm = Film.builder()
                .name(film.getName())
                .year(film.getYear())
                .description(film.getDescription())
                .state(Film.State.ACTUAL)
                .build();

        filmsRepository.save(newFilm);
    }

    @Override
    public Film getFilm(Long id) {
        return filmsRepository.findById(id).orElseThrow();
    }

    @Override
    public void updateFilm(Long id, FilmForm filmForm) {
        Film film = filmsRepository.findById(id).orElseThrow();

        film.setName(filmForm.getName());
        film.setYear(filmForm.getYear());
        film.setDescription(filmForm.getDescription());

        filmsRepository.save(film);
    }

    @Override
    public void deleteFilm(Long id) {
        Film film = filmsRepository.findById(id).orElseThrow();

        film.setState(Film.State.DELETED);

        filmsRepository.save(film);
    }
}
