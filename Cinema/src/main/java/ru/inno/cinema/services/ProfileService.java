package ru.inno.cinema.services;

import ru.inno.cinema.models.Playlist;
import ru.inno.cinema.models.User;
import ru.inno.cinema.security.details.CustomUserDetails;

import java.util.Set;

public interface ProfileService {
    User getCurrent(CustomUserDetails userDetails);

    void deletePlaylist(CustomUserDetails userDetails, Long playlistId);
}
