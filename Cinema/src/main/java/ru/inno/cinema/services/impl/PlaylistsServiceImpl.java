package ru.inno.cinema.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.inno.cinema.dto.PlaylistForm;
import ru.inno.cinema.models.Film;
import ru.inno.cinema.models.Playlist;
import ru.inno.cinema.models.User;
import ru.inno.cinema.repositories.FilmsRepository;
import ru.inno.cinema.repositories.PlaylistsRepository;
import ru.inno.cinema.repositories.UsersRepository;
import ru.inno.cinema.services.PlaylistsService;

import java.util.List;

@Service
@RequiredArgsConstructor
public class PlaylistsServiceImpl implements PlaylistsService {

    private final PlaylistsRepository playlistsRepository;
    private final UsersRepository usersRepository;
    private final FilmsRepository filmsRepository;

    @Override
    public List<Playlist> getAllPlaylists() {
        return playlistsRepository.findAllByStateNot(Playlist.State.DELETED);
    }

    @Override
    public void addPlaylist(PlaylistForm playlist) {
        Playlist newPlaylist = Playlist.builder()
                .title(playlist.getTitle())
                .author(usersRepository.findById(playlist.getAuthorId()).orElseThrow())
                .state(Playlist.State.ACTUAL)
                .build();

        playlistsRepository.save(newPlaylist);
    }

    @Override
    public Playlist getPlaylist(Long id) {
        return playlistsRepository.findById(id).orElseThrow();
    }

    @Override
    public void updatePlaylist(Long id, PlaylistForm playlistForm) {
        Playlist playlist = playlistsRepository.findById(id).orElseThrow();

        playlist.setTitle(playlistForm.getTitle());

        playlistsRepository.save(playlist);
    }

    @Override
    public void deletePlaylist(Long id) {
        Playlist playlist = playlistsRepository.findById(id).orElseThrow();

        playlist.setState(Playlist.State.DELETED);

        playlistsRepository.save(playlist);
    }

    @Override
    public List<Film> getFilmsInPlaylist(Long id) {
        Playlist playlist = playlistsRepository.findById(id).orElseThrow();

        return filmsRepository.findAllByPlaylistsContains(playlist);
    }

    @Override
    public List<Film> getFilmsNotInPlaylist(Long id) {
        Playlist playlist = playlistsRepository.findById(id).orElseThrow();

        return filmsRepository.findAllByPlaylistsNotContains(playlist);
    }

    @Override
    public void addFilmToPlaylist(Long playlistId, Long filmId) {
        Playlist playlist = playlistsRepository.findById(playlistId).orElseThrow();
        Film film = filmsRepository.findById(filmId).orElseThrow();

        film.getPlaylists().add(playlist);

        filmsRepository.save(film);
    }

    @Override
    public void deleteFilmFromPlaylist(Long playlistId, Long filmId) {
        Playlist playlist = playlistsRepository.findById(playlistId).orElseThrow();
        Film film = filmsRepository.findById(filmId).orElseThrow();

        film.getPlaylists().remove(playlist);

        filmsRepository.save(film);
    }

    @Override
    public void addPlaylistToUser(Long playlistId, Long userId) {
        Playlist playlist = playlistsRepository.findById(playlistId).orElseThrow();
        User user = usersRepository.findById(userId).orElseThrow();

        user.getPlaylists().add(playlist);

        usersRepository.save(user);
    }
}
