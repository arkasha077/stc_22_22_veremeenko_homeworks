package ru.inno.cinema.services;

import ru.inno.cinema.dto.PlaylistForm;
import ru.inno.cinema.models.Film;
import ru.inno.cinema.models.Playlist;
import ru.inno.cinema.models.User;

import java.util.List;

public interface PlaylistsService {
    List<Playlist> getAllPlaylists();

    void addPlaylist(PlaylistForm playlist);

    Playlist getPlaylist(Long id);

    void updatePlaylist(Long id, PlaylistForm playlist);

    void deletePlaylist(Long id);

    List<Film> getFilmsInPlaylist(Long id);

    List<Film> getFilmsNotInPlaylist(Long id);

    void addFilmToPlaylist(Long playlistId, Long filmId);

    void deleteFilmFromPlaylist(Long playlistId, Long filmId);

    void addPlaylistToUser(Long playlistId, Long userId);
}
