package ru.inno.cinema.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.inno.cinema.dto.UserForm;
import ru.inno.cinema.models.User;
import ru.inno.cinema.repositories.UsersRepository;
import ru.inno.cinema.services.UsersService;

import java.util.List;

@Service
@RequiredArgsConstructor
public class UsersServiceImpl implements UsersService {

    private final UsersRepository usersRepository;

    private final PasswordEncoder passwordEncoder;

    @Override
    public List<User> getAllUsers() {
        return usersRepository.findAllByStateNot(User.State.DELETED);
    }

    @Override
    public void addUser(UserForm user) {
        User newUser = User.builder()
                .email(user.getEmail())
                .password(passwordEncoder.encode(user.getPassword()))
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .age(user.getAge())
                .state(User.State.ACTUAL)
                .role(User.Role.USER)
                .build();

        usersRepository.save(newUser);
    }

    @Override
    public User getUser(Long id) {
        return usersRepository.findById(id).orElseThrow();
    }

    @Override
    public void updateUser(Long userId, UserForm userForm) {
        User user = usersRepository.findById(userId).orElseThrow();

        user.setFirstName(userForm.getFirstName());
        user.setLastName(userForm.getLastName());
        user.setAge(userForm.getAge());

        usersRepository.save(user);
    }

    @Override
    public void deleteUser(Long id) {
        User user = usersRepository.findById(id).orElseThrow();

        user.setState(User.State.DELETED);

        usersRepository.save(user);
    }
}
