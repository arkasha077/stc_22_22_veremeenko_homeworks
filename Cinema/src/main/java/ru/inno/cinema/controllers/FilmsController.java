package ru.inno.cinema.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import ru.inno.cinema.dto.FilmForm;
import ru.inno.cinema.security.details.CustomUserDetails;
import ru.inno.cinema.services.FilmsService;

@RequiredArgsConstructor
@Controller
public class FilmsController {

    private final FilmsService filmsService;

    @GetMapping("/films")
    public String getFilmsPage(@AuthenticationPrincipal CustomUserDetails customUserDetails, Model model) {
        model.addAttribute("films", filmsService.getAllFilms());
        model.addAttribute("userRole", customUserDetails.getUserRole());
        return "films/films_page";
    }

    @PostMapping("/films")
    public String addFilm(FilmForm film) {
        filmsService.addFilm(film);
        return "redirect:/films";
    }

    @GetMapping("/films/{film-id}")
    public String getFilmPage(@AuthenticationPrincipal CustomUserDetails customUserDetails,
                              @PathVariable("film-id") Long id,
                              Model model) {
        model.addAttribute("film", filmsService.getFilm(id));
        model.addAttribute("userRole", customUserDetails.getUserRole());
        return "films/film_page";
    }

    @PostMapping("/films/{film-id}/update")
    public String updateFilm(@PathVariable("film-id") Long id, FilmForm film) {
        filmsService.updateFilm(id, film);
        return "redirect:/films/" + id;
    }

    @GetMapping("/films/{film-id}/delete")
    public String deleteFilm(@PathVariable("film-id") Long id) {
        filmsService.deleteFilm(id);
        return "redirect:/films/";
    }
}
