package ru.inno.cinema.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import ru.inno.cinema.dto.UserForm;
import ru.inno.cinema.services.UsersService;

@RequiredArgsConstructor
@Controller
public class UsersController {

    private final UsersService usersService;

    @GetMapping("/users")
    public String getUsersPage(Model model) {
        model.addAttribute("users", usersService.getAllUsers());
        return "users/users_page";
    }

    @PostMapping("/users")
    public String addUser(UserForm user) {
        usersService.addUser(user);
        return "redirect:/users";
    }

    @GetMapping("/users/{user-id}")
    public String getUserPage(@PathVariable("user-id") Long id, Model model) {
        model.addAttribute("user", usersService.getUser(id));
        return  "users/user_page";
    }

    @PostMapping("/users/{user-id}/update")
    public String updateUser(@PathVariable("user-id") Long id, UserForm user) {
        usersService.updateUser(id, user);
        return "redirect:/users/" + id;
    }

    @GetMapping("/users/{user-id}/delete")
    public String deleteUser(@PathVariable("user-id") Long id) {
        usersService.deleteUser(id);
        return "redirect:/users/";
    }
}
