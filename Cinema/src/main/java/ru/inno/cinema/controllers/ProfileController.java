package ru.inno.cinema.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import ru.inno.cinema.security.details.CustomUserDetails;
import ru.inno.cinema.services.ProfileService;

@RequiredArgsConstructor
@Controller
public class ProfileController {
    private final ProfileService profileService;

    @GetMapping("/")
    public String getRoot() {
        return "redirect:/profile";
    }

    @GetMapping("/profile")
    public String getProfilePage(@AuthenticationPrincipal CustomUserDetails customUserDetails, Model model) {
        model.addAttribute("user", profileService.getCurrent(customUserDetails));
        model.addAttribute("playlists", profileService.getCurrent(customUserDetails).getPlaylists());
        return "users/profile_page";
    }

    @GetMapping("/profile/playlists/{playlist-id}/delete")
    public String deletePlaylist(@AuthenticationPrincipal CustomUserDetails customUserDetails,
                                     @PathVariable("playlist-id") Long playlistId) {

        profileService.deletePlaylist(customUserDetails, playlistId);
        return "redirect:/profile/";
    }
}
