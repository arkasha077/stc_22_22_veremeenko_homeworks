package ru.inno.cinema.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.inno.cinema.dto.PlaylistForm;
import ru.inno.cinema.security.details.CustomUserDetails;
import ru.inno.cinema.services.PlaylistsService;

@RequiredArgsConstructor
@Controller
public class PlaylistsController {

    private final PlaylistsService playlistsService;

    @GetMapping("/playlists")
    public String getPlaylistsPage(Model model) {
        model.addAttribute("playlists", playlistsService.getAllPlaylists());
        return "playlists/playlists_page";
    }

    @PostMapping("/playlists")
    public String addPlaylist(@AuthenticationPrincipal CustomUserDetails customUserDetails, PlaylistForm playlist) {
        playlist.setAuthorId(customUserDetails.getUserId());
        playlistsService.addPlaylist(playlist);
        return "redirect:/playlists";
    }

    @GetMapping("/playlists/{playlist-id}")
    public String getPlaylistPage(@PathVariable("playlist-id") Long id, Model model) {
        model.addAttribute("playlist", playlistsService.getPlaylist(id));
        model.addAttribute("filmsInPlaylist", playlistsService.getFilmsInPlaylist(id));
        model.addAttribute("filmsNotInPlaylist", playlistsService.getFilmsNotInPlaylist(id));
        return "playlists/playlist_page";
    }

    @PostMapping("/playlists/{playlist-id}/update")
    public String updatePlaylist(@AuthenticationPrincipal CustomUserDetails customUserDetails,
                                 @PathVariable("playlist-id") Long id,
                                 PlaylistForm playlist) {

        if (playlistsService.getPlaylist(id).getAuthor().getId().equals(customUserDetails.getUserId())) {
            playlistsService.updatePlaylist(id, playlist);
        }
        return "redirect:/playlists/" + id;
    }

    @GetMapping("/playlists/{playlist-id}/delete")
    public String deletePlaylist(@AuthenticationPrincipal CustomUserDetails customUserDetails,
                                 @PathVariable("playlist-id") Long id) {

        if (playlistsService.getPlaylist(id).getAuthor().getId().equals(customUserDetails.getUserId())) {
            playlistsService.deletePlaylist(id);
        }
        return "redirect:/playlists/";
    }

    @PostMapping("/playlists/{playlist-id}/films")
    public String addFilmInPlaylist(@AuthenticationPrincipal CustomUserDetails customUserDetails,
                                    @PathVariable("playlist-id") Long playlistId,
                                    @RequestParam("film-id") Long filmId) {

        if (playlistsService.getPlaylist(playlistId).getAuthor().getId().equals(customUserDetails.getUserId())) {
            playlistsService.addFilmToPlaylist(playlistId, filmId);
        }
        return "redirect:/playlists/" + playlistId;
    }

    @GetMapping("/playlists/{playlist-id}/films/{film-id}/delete")
    public String deleteFilmFromPlaylist(@AuthenticationPrincipal CustomUserDetails customUserDetails,
                                    @PathVariable("playlist-id") Long playlistId,
                                    @PathVariable("film-id") Long filmId) {

        if (playlistsService.getPlaylist(playlistId).getAuthor().getId().equals(customUserDetails.getUserId())) {
            playlistsService.deleteFilmFromPlaylist(playlistId, filmId);
        }
        return "redirect:/playlists/" + playlistId;
    }

    @GetMapping("/playlists/{playlist-id}/add")
    public String addPlaylistToUser(@AuthenticationPrincipal CustomUserDetails customUserDetails,
                                    @PathVariable("playlist-id") Long playlistId) {

        playlistsService.addPlaylistToUser(playlistId, customUserDetails.getUserId());
        return "redirect:/playlists/" + playlistId;
    }
}
