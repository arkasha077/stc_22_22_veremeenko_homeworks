package ru.inno.cinema.models;

import javax.persistence.*;

import lombok.*;

import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@EqualsAndHashCode(exclude = "playlists")
@ToString(exclude = "playlists")
public class Film {

    public enum State {
        ACTUAL, DELETED
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String name;

    private Integer year;

    @Column(length = 5000)
    private String description;

    @ManyToMany
    @JoinTable(joinColumns = {@JoinColumn(name = "film_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "playlist_id", referencedColumnName = "id")})
    private Set<Playlist> playlists;

    @Enumerated(value = EnumType.STRING)
    private State state;
}
