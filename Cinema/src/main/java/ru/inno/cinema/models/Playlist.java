package ru.inno.cinema.models;

import javax.persistence.*;
import lombok.*;

import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode(exclude = {"author", "films", "accounts"})
@Entity
public class Playlist {

    public enum State {
        ACTUAL, DELETED
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 30)
    private String title;

    @ManyToOne
    @JoinColumn(name = "author_id", nullable = false)
    private User author;

    @ManyToMany(mappedBy = "playlists", fetch = FetchType.EAGER)
    private Set<Film> films;

    @ManyToMany(mappedBy = "playlists", fetch = FetchType.EAGER)
    private Set<User> accounts;

    @Enumerated(value = EnumType.STRING)
    private State state;
}
