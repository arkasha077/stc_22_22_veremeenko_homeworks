package ru.inno.cinema.models;

import javax.persistence.*;
import lombok.*;
import org.hibernate.annotations.Check;

import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(exclude = {"playlists"})
@ToString(exclude = {"playlists"})
@Builder
@Entity
@Table(name = "account")
public class User {

    public enum State {
        ACTUAL, DELETED
    }

    public enum Role {
        USER, ADMIN
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true, nullable = false)
    private String email;

    private String password;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "age")
    @Check(constraints = "age >= 0 AND age <= 120")
    private Integer age;

    @ManyToMany
    @JoinTable(joinColumns = {@JoinColumn(name = "account_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "playlist_id", referencedColumnName = "id")})
    private Set<Playlist> playlists;

    @Enumerated(value = EnumType.STRING)
    private State state;

    @Enumerated(value = EnumType.STRING)
    private Role role;
}
