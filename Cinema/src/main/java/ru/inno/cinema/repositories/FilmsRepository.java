package ru.inno.cinema.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.inno.cinema.models.Film;
import ru.inno.cinema.models.Playlist;

import java.util.List;

public interface FilmsRepository extends JpaRepository<Film, Long> {
    List<Film> findAllByStateNot(Film.State state);

    List<Film> findAllByPlaylistsContains(Playlist playlist);

    List<Film> findAllByPlaylistsNotContains(Playlist playlist);
}
