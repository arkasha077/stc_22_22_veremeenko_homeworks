package ru.inno.cinema.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.inno.cinema.models.Playlist;

import java.util.List;

public interface PlaylistsRepository extends JpaRepository<Playlist, Long> {
    List<Playlist> findAllByStateNot(Playlist.State state);
}
