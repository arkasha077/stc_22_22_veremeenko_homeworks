package ru.inno.cinema.security.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.rememberme.JdbcTokenRepositoryImpl;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;

import javax.sql.DataSource;

@EnableWebSecurity
public class SecurityConfig {

    @Autowired
    private UserDetailsService customUserDetailsService;

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity httpSecurity, PersistentTokenRepository tokenRepository) throws Exception {
        httpSecurity.csrf().disable();

        httpSecurity.formLogin(Customizer.withDefaults());
        httpSecurity.authorizeHttpRequests().antMatchers("/users/**").hasAuthority("ADMIN");
        httpSecurity.authorizeHttpRequests().antMatchers(HttpMethod.GET, "/films").hasAnyAuthority("ADMIN", "USER");
        httpSecurity.authorizeHttpRequests().antMatchers(HttpMethod.POST, "/films").hasAuthority("ADMIN");
        httpSecurity.authorizeHttpRequests().antMatchers(HttpMethod.GET, "/films/*").hasAnyAuthority("ADMIN", "USER");
        httpSecurity.authorizeHttpRequests().antMatchers(HttpMethod.POST, "/films/*").hasAuthority("ADMIN");
        httpSecurity.authorizeHttpRequests().antMatchers("/films/*/*").hasAuthority("ADMIN");
        httpSecurity.authorizeHttpRequests().antMatchers("/playlists/**").hasAnyAuthority("ADMIN", "USER");
        httpSecurity.authorizeHttpRequests().antMatchers("/profile/**").hasAnyAuthority("ADMIN", "USER");

        return httpSecurity.build();
    }

    @Autowired
    public void bindUserDetailsService(AuthenticationManagerBuilder builder,
                                       PasswordEncoder passwordEncoder) throws Exception {
        builder.userDetailsService(customUserDetailsService)
                .passwordEncoder(passwordEncoder);
    }

    @Bean
    public PersistentTokenRepository tokenRepository(DataSource dataSource) {
        JdbcTokenRepositoryImpl jdbcTokenRepository = new JdbcTokenRepositoryImpl();
        jdbcTokenRepository.setDataSource(dataSource);
        return jdbcTokenRepository;
    }
}
