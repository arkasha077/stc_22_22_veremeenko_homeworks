public class Main {
    public static void main(String[] args) {
        //проверка remove у ArrayList
        ArrayList<Integer> integerArrayList = new ArrayList<>();

        integerArrayList.add(1);
        integerArrayList.add(2);
        integerArrayList.add(3);
        integerArrayList.add(4);
        integerArrayList.add(5);
        integerArrayList.add(6);

        System.out.print("\nArrayList");

        printCollection(integerArrayList);

        integerArrayList.removeAt(2);
        integerArrayList.remove(1);
        integerArrayList.remove(6);
        integerArrayList.add(7);
        integerArrayList.add(8);

        printCollection(integerArrayList);

        //проверка remove у LinkedList
        LinkedList<Integer> integerLinkedList = new LinkedList<>();

        integerLinkedList.add(1);
        integerLinkedList.add(2);
        integerLinkedList.add(3);
        integerLinkedList.add(4);
        integerLinkedList.add(5);
        integerLinkedList.add(6);

        System.out.print("\nLinkedList");

        printCollection(integerLinkedList);

        integerLinkedList.removeAt(3);
        integerLinkedList.removeAt(0);
        integerLinkedList.remove(6);

        printCollection(integerLinkedList);

        integerLinkedList.add(7);
        integerLinkedList.add(8);
        integerLinkedList.add(9);

        printCollection(integerLinkedList);

        integerLinkedList.remove(2);
        integerLinkedList.remove(7);

        printCollection(integerLinkedList);
    }

    public static void printCollection(Collection collection) {
        Iterator<Integer> integerIterator = collection.iterator();

        System.out.println();

        while (integerIterator.hasNext()) {
            System.out.print(integerIterator.next() + " ");
        }
    }
}