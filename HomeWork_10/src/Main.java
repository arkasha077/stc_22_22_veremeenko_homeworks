import java.util.*;

public class Main {
    public static void main(String[] args) {
        String string = "Hello Hello bye Hello bye Inno";

        Map<String, Integer> map = toMap(string);

        Integer max = Collections.max(map.values());
        String key = getKey(map, max);

        System.out.printf("\n%s %d", key, max);
    }

    public static Map<String, Integer> toMap(String string) {
        String[] words = string.split(" ");

        Map<String, Integer> map = new HashMap<>();

        for (String word : words) {
            if (map.keySet().contains(word)) {
                map.put(word, map.get(word) + 1);
            } else {
                map.put(word, 1);
            }
        }

        return map;
    }

    public static String getKey(Map<String, Integer> map, Integer value) {
        Set<Map.Entry<String, Integer>> entrySet = map.entrySet();

        for (Map.Entry<String, Integer> pair : entrySet) {
            if (value.equals(pair.getValue())) {
                return pair.getKey();
            }
        }

        return null;
    }
}