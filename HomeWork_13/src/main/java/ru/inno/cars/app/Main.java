package ru.inno.cars.app;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import jakarta.persistence.EntityManager;
import lombok.experimental.ExtensionMethod;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import ru.inno.cars.extensions.ExtensionScanner;
import ru.inno.cars.models.Car;
import ru.inno.cars.repository.CarRepository;
import ru.inno.cars.repository.CarRepositoryJpaImpl;

import java.util.Scanner;

@ExtensionMethod({ExtensionScanner.class})
public class Main {
    @Parameter(names = {"--action"})
    private String action;

    public static void main(String[] args) {
        Main program = new Main();

        JCommander.newBuilder()
                .addObject(program)
                .build()
                .parse(args);

        Configuration configuration = new Configuration();
        configuration.configure();

        SessionFactory sessionFactory = configuration.buildSessionFactory();

        EntityManager entityManager = sessionFactory.createEntityManager();

        CarRepository cars = new CarRepositoryJpaImpl(entityManager);

        if (program.action.equalsIgnoreCase("read")) read(cars);

        if (program.action.equalsIgnoreCase("write")) write(cars);
    }

    static Car getCarFromScanner(Scanner scanner) {
        String model = scanner.nextLineWithMessage("\nEnter car model: ");
        String color = scanner.nextLineWithMessage("\nEnter car color: ");

        return Car.builder()
                .model(model)
                .color(color)
                .build();
    }

    static void write(CarRepository cars) {
        Scanner scanner = new Scanner(System.in);

        while (!scanner.nextLineWithMessage("\nPress enter to continue or print 'exit' to leave: ").equalsIgnoreCase("exit")) {
            cars.save(getCarFromScanner(scanner));
        }
    }

    static void read(CarRepository cars) {
        for (Car car : cars.findAll()) {
            System.out.println(car.toString());
        }
    }
}