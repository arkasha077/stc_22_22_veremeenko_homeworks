package ru.inno.cars.extensions;

import java.util.Scanner;

public class ExtensionScanner {
    public static String nextLineWithMessage(Scanner scanner, String message) {
        System.out.print(message);
        return scanner.nextLine();
    }
}
