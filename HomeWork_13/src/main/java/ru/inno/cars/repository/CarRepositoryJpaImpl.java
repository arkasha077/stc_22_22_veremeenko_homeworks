package ru.inno.cars.repository;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityTransaction;
import lombok.RequiredArgsConstructor;
import ru.inno.cars.models.Car;

import java.util.List;

@RequiredArgsConstructor
public class CarRepositoryJpaImpl implements CarRepository {

    private final EntityManager entityManager;

    @Override
    public List<Car> findAll() {
        return entityManager.createQuery("select car from Car car", Car.class).getResultList();
    }

    @Override
    public void save(Car car) {
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        entityManager.persist(car);
        transaction.commit();
    }
}
