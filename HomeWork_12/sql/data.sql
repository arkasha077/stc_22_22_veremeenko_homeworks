insert into driver (first_name, last_name, phone_number, experience, age, is_have_driver_license, license_category, rate)
values ('Андрей', 'Сивцев', '+79991234556', 4, 23, true, 'A1', 4),
       ('Максим', 'Иванов', '+79138881234', 0, 18, true, 'B', 0),
       ('Олег', 'Лололо', '+79798521447', 25, 45, true, 'BE', 5),
       ('Кирилл', 'Алексеев', '+79246663258', 1, 21, true, 'B', 3);

insert into driver (first_name, last_name, phone_number, age)
values ('Денис', 'Ложкин', '+79967891452', 29);

insert into car (model, color, number, driver_id)
values ('Honda', 'Черный', 'А123ВВ', 1),
       ('Toyota', 'Белый', 'А695КА', 2),
       ('Mazda', 'Розовый', 'А123БВ', 3),
       ('Lexus', 'Синий', 'Н100ЕН', 4),
       ('Subaru', 'Коричневый', 'К999КК', 5);

insert into driver_car (driver_id, car_id)
values (1, 1),
       (2, 2),
       (3, 3),
       (4, 4),
       (5, 5);