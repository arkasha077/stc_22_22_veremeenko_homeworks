drop table if exists driver_car;
drop table if exists car;
drop table if exists driver;

create table driver (
                        id bigserial primary key,
                        first_name char(20) not null,
                        last_name char(20) not null,
                        phone_number char(20) unique,
                        experience integer check (experience >= 0 and experience <= 102) default 0,
                        age integer check (age >= 0 and age <= 120) not null,
                        is_have_driver_license bool default false,
                        license_category char(3) default 'no',
                        rate integer check (rate >= 0 and rate <= 5) default 0
);

create table car (
                     id bigserial primary key,
                     model char(50) not null,
                     color char(20) default 'DEFAULT',
                     number char(6) not null,
                     driver_id bigint not null,
                     foreign key (driver_id) references driver (id)
);

create table driver_car (
                            id bigserial primary key,
                            driver_id bigint not null,
                            car_id bigint not null,
                            foreign key (driver_id) references driver (id),
                            foreign key (car_id) references car (id)
);